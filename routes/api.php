<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\MultipleFileController;
use App\Http\Controllers\Location\CountryController;
use App\Http\Controllers\Location\DistrictController;
use App\Http\Controllers\Location\DivisionController;
use App\Http\Controllers\Location\ThanaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::post('/register', 'App\Http\Controllers\AuthController@register');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/register',[AuthController::class,'register']);
Route::post('/login',[AuthController::class,'login']);
Route::group(['middleware'=>['auth:sanctum']],function(){

    Route::post('/logout',[AuthController::class,'logout']);

});

Route::post('/multiple-file', [MultipleFileController::class, 'store']);
Route::get('/multiple-file-view', [MultipleFileController::class, 'index']);
Route::delete('/multiple-file-delete/{id}', [MultipleFileController::class, 'delete']);

Route::apiResources([
    'blog' => BlogController::class,
    'country' => CountryController::class,
    'division' => DivisionController::class,
    'district' => DistrictController::class,
    'thana' => ThanaController::class,
    ]
);
