<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Thana extends Model
{
    use HasFactory;
    protected $table = 'thanas';
    protected $fillable = [
        'name',
        'district_id'
    ];

    protected $primary_key = 'id';
    public $timestamps = false;

    public function districts()
    {
        return $this->belongsTo(District::class, 'district_id', 'id');
    }
}
