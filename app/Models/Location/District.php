<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory;
    protected $table = 'districts';
    protected $fillable = [
        'name',
        'division_id'
    ];

    protected $primary_key = 'id';
    public $timestamps = false;

    public function divisions()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function thanas()
    {
        return $this->hasMany(Thana::class, 'district_id', 'id');
    }
}
