<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;
    protected $table = 'countries';

    protected $fillable = [
        'name',
    ];

    protected $primary_key = 'id';
    public $timestamps = false;


    public function divisions()
    {
        return $this->hasMany(Division::class, 'country_id', 'id');
    }


}
