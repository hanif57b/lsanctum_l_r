<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MultipleFile extends Model
{
    use HasFactory;
    protected $table="multiple_files";
    protected $fillable = [
        'fileone',
        'filetwo',
        'filethree',
        'filefour',
    ];

    // public function setFilenamesAttribute($value)
    // {
    //     $this->attributes['file'] = json_encode($value);
    // }
}
