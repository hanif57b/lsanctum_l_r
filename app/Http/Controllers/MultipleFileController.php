<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Validator;
use App\Models\MultipleFile;

class MultipleFileController extends Controller
{

    public function index(){
        return MultipleFile::get();
    }

    public function store(Request $request)
    {
        $create  = new MultipleFile();;
            if ( $request->hasFile( 'fileone' ) ) {
                $image = $request->file( 'fileone' );
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make( $image )->resize( 400, 380 )->save( public_path( 'multiple/fileone/' . $filename ) );
                $create->fileone = 'multiple/fileone/'.$filename;
            }
            if ( $request->hasFile( 'filetwo' ) ) {
                $image = $request->file( 'filetwo' );
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make( $image )->resize( 400, 380 )->save( public_path( 'multiple/filetwo/' . $filename ) );
                $create->filetwo = 'multiple/filetwo/'.$filename;
            }
            if ( $request->hasFile( 'filethree' ) ) {
                $image = $request->file( 'filethree' );
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make( $image )->resize( 400, 380 )->save( public_path( 'multiple/filethree/' . $filename ) );
                $create->filethree = 'multiple/filethree/'.$filename;
            }
            if ( $request->hasFile( 'filefour' ) ) {
                $image = $request->file( 'filefour' );
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make( $image )->resize( 400, 380 )->save( public_path( 'multiple/filefour/' . $filename ) );
                $create->filefour = 'multiple/filefour/'.$filename;
            }
            $create->save();
            return response()->json(['success'=>'true','message'=>' Upload sucessfully'],201);

    }

    public function delete($id){
        $files = MultipleFile::findOrFail($id);
        $files->delete();
        return response()->json([
            'status_code' => 200,
            'message' => $files

        ]);
    }


}
