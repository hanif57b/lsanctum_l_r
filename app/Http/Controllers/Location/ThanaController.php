<?php

namespace App\Http\Controllers\Location;

use App\Http\Controllers\Controller;
use App\Models\Location\District;
use Illuminate\Http\Request;
use App\Models\Location\Thana;

class ThanaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Thana::orderby('name')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:divisions|max:50',
            'district_id' => 'required|integer',
        ]);

        return Thana::create([
            'name' => $request['name'],
            'district_id' => $request['district_id'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Thana::where('id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Thana $thana)
    {
        return Thana::find($thana->id)->orderby('name')->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thana $thana)
    {
        $query = Thana::findOrFail($thana->id);
        $this->validate($request, [
            'name' => 'required|string|unique:divisions,name|max:50'.$thana->id,
            'district_id' => 'required|integer',
        ]);

        $query->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Thana $thana)
    {
        $query = Thana::findOrFail($thana->id);
        $query->delete();
    }
}
