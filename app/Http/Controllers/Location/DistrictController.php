<?php

namespace App\Http\Controllers\Location;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Location\District;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return District::orderby('name')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:divisions|max:50',
            'division_id' => 'required|integer',
        ]);

        return District::create([
            'name' => $request['name'],
            'division_id' => $request['division_id'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return District::where('id', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(District $district)
    {
        return District::find($district->id)->orderby('name')->get();
    }

    /**
     * Get Thana by District Id
     */

    public function thanasByDistrict($id)
    {
        $query = District::findOrFail($id);
        return $query->thanas()->orderBy('name')->get();


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    }
    public function update(Request $request, District $district)
    {
        $query = District::findOrFail($district->id);
        $this->validate($request, [
            'name' => 'required|string|unique:divisions,name|max:50'.$district->id,
            'division_id' => 'required|integer',
        ]);

        $query->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(District $district)
    {
        $query = District::findOrFail($district->id);
        $query->delete();
    }
}
